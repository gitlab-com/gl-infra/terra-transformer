#!/usr/bin/env bash

testDryRun() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  ../terra-transformer protected-apply --chdir fixtures/terraform1 --dry-run
  assertEquals "terra-transformer protected-apply --dry-run should have returned a zero exit code" "$?" 0
  assertFalse "fixtures/terraform1/.terra-apply-protected-executed should not exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
}

testRunNoProtection() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  ../terra-transformer protected-apply --chdir fixtures/terraform1
  assertEquals "terra-transformer protected-apply --dry-run should have returned a zero exit code" "$?" 0
  assertTrue "fixtures/terraform1/.terra-apply-protected-executed should exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
}

testProtectProtectedResources() {
  # Before running protect, run without config to ensure that the
  # item is definitely there
  ../terra-transformer protected-apply --chdir fixtures/terraform1

  rm -rf fixtures/terraform1/.terra-apply-protected-executed

  ../terra-transformer --config example_config.yml protected-apply --chdir fixtures/terraform1 && {
    fail "terra-transformer protected-apply should have returned with an error-code"
  }

  assertFalse "fixtures/terraform1/.terra-apply-protected-executed should not exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
}

testApply() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  ../terra-transformer protected-apply --chdir fixtures/terraform1
  assertEquals "terra-transformer protected-apply should have returned with exit code 0" "$?" 0
  assertTrue "fixtures/terraform1/.terra-apply-protected-executed should exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'

  # Cleanup
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
}

testApplyWithJson() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  rm -rf protected-apply.log
  ../terra-transformer protected-apply --chdir fixtures/terraform1 --json >protected-apply.log
  assertEquals "terra-transformer protected-apply should have returned with exit code 0" "$?" 0
  jq . protected-apply.log
  assertEquals "jq exit 0 indicating valid json" "$?" 0

  # Cleanup
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  rm -rf protected-apply.log
}

testDryRunPlanFile() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  rm -rf test_plan.json
  ../terra-transformer protected-apply --chdir fixtures/terraform1 --dry-run --plan-file-json test_plan.json
  assertEquals "terra-transformer protected-apply --dry-run --plan-file-json ... should have returned a zero exit code" "$?" 0
  assertFalse "fixtures/terraform1/.terra-apply-protected-executed should not exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
  assertTrue "test_plan.json should exist" '[ -f test_plan.json ]'

  # Cleanup
  rm -rf test_plan.json
}

testApplyWithPlanFile() {
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  rm -rf test_plan.json
  ../terra-transformer protected-apply --chdir fixtures/terraform1 --plan-file-json test_plan.json
  assertEquals "terra-transformer protected-apply --plan-file-json ... should have returned a zero exit code" "$?" 0
  assertTrue "fixtures/terraform1/.terra-apply-protected-executed should exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
  assertTrue "test_plan.json should exist" '[ -f test_plan.json ]'

  # Cleanup
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  rm -rf test_plan.json
}

testAllowExemption() {
  midnight=$(date +"%Y-%m-%dT00:00:00Z")
  exemption=${midnight},24h=null_resource
  echo "Using exemption: ${exemption}"

  rm -rf fixtures/terraform1/.terra-apply-protected-executed
  TERRA_PROTECT_RESOURCE_EXEMPTION=${exemption} ../terra-transformer --config example_config.yml protected-apply --chdir fixtures/terraform1
  assertEquals "exemption should have been allowed" "$?" 0
  assertTrue "fixtures/terraform1/.terra-apply-protected-executed should exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'

  # Cleanup
  rm -rf fixtures/terraform1/.terra-apply-protected-executed
}

testDenyExpiredExemption() {
  exemption=2021-05-12T00:00:00Z,24h=null_resource

  rm -rf fixtures/terraform1/.terra-apply-protected-executed

  TERRA_PROTECT_RESOURCE_EXEMPTION=${exemption} ../terra-transformer --config example_config.yml protected-apply --chdir fixtures/terraform1 && {
    fail "expired exemption (${exemption}) should have been denied"
  }
  assertFalse "fixtures/terraform1/.terra-apply-protected-executed should not exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
}

testDenyInvalidExemption() {
  exemption=invalid=null_resource

  rm -rf fixtures/terraform1/.terra-apply-protected-executed

  TERRA_PROTECT_RESOURCE_EXEMPTION=${exemption} ../terra-transformer --config example_config.yml protected-apply --chdir fixtures/terraform1 && {
    fail "invalid exemption (${exemption}) should have been denied"
  }
  assertFalse "fixtures/terraform1/.terra-apply-protected-executed should not exist" '[ -f fixtures/terraform1/.terra-apply-protected-executed ]'
}

# Load shUnit2.

# shellcheck source=/dev/null
. ./lib/shunit2
