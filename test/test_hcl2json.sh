#!/usr/bin/env bash

testHCL2JSonSimpleHCL() {
  output=$(../terra-transformer hcl2json <fixtures/hcl2json/simple.hcl)
  assertEquals "terra-transformer hcl2json should return a zero exit code" "$?" 0
  # ci container doesn't have jq, so use string parsing on the output
  assertContains "hcl2json output did not contain expected key/value pair" "$output" '"key":"value"'
}

testHCL2JSonComplexHCL() {
  output=$(../terra-transformer hcl2json <fixtures/hcl2json/complex.hcl)
  assertEquals "terra-transformer hcl2json should return a zero exit code" "$?" 0
}

testHCL2JsonInvalidInputFails() {
  ../terra-transformer hcl2json <fixtures/hcl2json/invalid.txt &>/dev/null
  assertNotEquals "terra-transformer hcl2json should return a non-zero exit code" "$?" 0
}

# Load shUnit2.

# shellcheck source=/dev/null
. ./lib/shunit2
