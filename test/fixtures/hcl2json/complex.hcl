complex_hcl_type = {
  map_type_subkey = {
    name = "some value"
  },
  array_type_subkey = ["value1", "value2"]
  simple_kv = "some value"
}
map_hcl_type = {
  foo = "bar"
}
array_hcl_type = ["foo", "bar", "baz"]
