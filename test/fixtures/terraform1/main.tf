terraform {
  required_providers {
    null = {
      source  = "hashicorp/null"
      version = "3.1.1"
    }
  }
  required_version = ">= 1.0"
}

provider "null" {
}

resource "null_resource" "resource" {
  triggers = {
    time = timestamp()
  }

  provisioner "local-exec" {
    command = "touch .terra-apply-protected-executed"
  }
}
