# Terra-Transformer

A small program to help import resources into Terraform in a consistent manner.

## Sub-Commands

### `import`

**Note**: When possible, use [Terraform `import` blocks](https://developer.hashicorp.com/terraform/language/import) instead of `terra-transformer import`. This is only possible if the imported resource is known to already exist.


`terra-transformer import` combines planned output values from a Terraform plan output along with Cloud SDK lookups to generate idempotent,
easy and safe `terraform import` commands.

Since it uses plan values, it can safely import resources based on Terraform variables.

**NOTE**: `import` assumes that re-imported resources intend to be used.
In some circumstances, `terra-transformer import` will perform necessary remediation
in order to ensure that resources are fit for purpose.

For example, when importing `google_kms_crypto_key` resources, the importer will validate that the primary key is enabled,
and has not been deleted. If this is not the case, the importer will restore the key and enable it.

#### Arguments

1. `--plan-file <plan_file>`: plan JSON file to use as input.
1. `--import-arguments` additional arguments to pass to the `terraform import` command.
1. (positional arguments) `args`: Terraform addresses of resources to import

#### Process

For each address:

1. If the address exists in the Prior State of the Terraform JSON, skip the resource as it's already imported.
1. If the address doesn't exist in the Planned Values of the Terraform JSON, throw an error as address doesn't exist.
1. If `terra-transformer` doesn't handle the type, throw an error.
1. Using the planned values, lookup the import key from the Cloud SDK. For example, for type `aws_dynamodb_table`, it uses the Cloud SDK to lookup the table name using the planned value's `name` attribute.
1. If the resource is found via the Cloud SDK, perform a `terraform import` of the resource.
1. If the resource is not found via the Cloud SDK, skip the `terraform import` and allow `terraform apply` to create the resource in a subsequent invocation.

#### Example

```shell
terra-transformer import \
  --plan-file "${ROOT_DIR}/main.tfplan.json" \
  --import-arguments "
      -var-file '$(pwd)/environments/${AMP_ENVIRONMENT}/common.hcl'
      -var-file '$(pwd)/environments/${AMP_ENVIRONMENT}/main.hcl'
      -var-file '$(pwd)/environments/${AMP_ENVIRONMENT}/overrides.json'
    " \
    "module.root_domain.aws_iam_policy.dns_control" \
    "module.root_domain.aws_iam_role.dns_control" \
    "module.root_domain.aws_route53_zone.zone"
```

#### Caveats for Importing Resources

##### `aws_kms_key`

`aws_kms_key` does not have a unique identifier, so the importer will find all keys with the following properties:

1. `KeyState` of `Enabled`
1. Description matches planned Description

If multiple keys match, the oldest key will be imported.


### `hcl2json`

`terra-transformer hcl2json` will convert an HCL file into JSON. This can be useful if the HCL is required to be machine readable, or ingested by tools such as `jq`, etc.

```shell
terra-transformer hcl2json <file.hcl >file.json
```

### `rm`

**Note**: When possible, use [Terraform `removed` blocks](https://developer.hashicorp.com/terraform/language/resources/syntax#removing-resources) instead of `terra-transformer rm`.

The `rm` subcommand will perform an idempotent removal of an address from the Terraform state. It does this by first checking that the items are in the state file, and issuing a `terraform state rm` only if they are.

`terra-transformer rm --plan-file plan.json addr [addr..]`

```shell
terra-transformer rm --plan-file main.tfplan.json module.roles.aws_iam_role.onboarding --dry-run
terra-transformer rm --plan-file main.tfplan.json module.roles.aws_iam_role.onboarding
```

### `protect`

Given a plan JSON file, `protect` will review all changes in the plan and fail if any "protected" resources are planned to be deleted. At present, the only protection filter is `--type` specifying a Terraform Resource type. The `--type` parameter can be added multiple times. If _any_ of the types are discovered, the command will return an error.

In future, other selectors (such as [tags](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/team/-/issues/660)) may be added additionally.

```console
$ terra-transformer protect --plan-file main.tfplan.json  --type aws_db_instance --type aws_eks_cluster
-------------------------------------------------------------------------
⚠️  Terraform Plan contains changes that will delete protected resources.

The following protected resources will be deleted:

- module.arch.aws_db_instance.gitlab[0]

⚠️  This may lead to loss of critical data. Manual intervention required.
-------------------------------------------------------------------------
```

### Shared configuration

If the global `--config <file>` flag is passed into `terra-transform`, the types from `protected_types` will be merged into the arguments provided via `--types`.

The relevant part of the configuration file looks like this:

```yaml
# Types that should be protected. The `terra-transform protect` tool will fail if any
# of these types are marked for destroy
protected_types:
- aws_backup_vault
- aws_db_instance
- aws_ebs_volume
- aws_eip
```

### Exemptions

Sometimes it's necessary to provide an exemption to bypass protect for certain resources which will be intentionally destroyed.

Note, exemptions work the same for both the `protect` and the `protected-apply` subcommands.

Exemptions can be specified resources with a specific address.

#### Exemptions stored in files in the repository with `--exemptions-dir`

Specific exemptions for resources, based on their (full) address can be stored in an exemptions directory.

This directory stores one or more files containing the addresses of resources that will not be protected in the event of their destruction.

This example illustrates the format of the file:

```yaml
exemptions:
- aws_secretsmanager_secret.managed_config_items["aws_elasticache_primary_endpoint_address"]
- aws_secretsmanager_secret.managed_config_items["elastic_ip"]
- aws_secretsmanager_secret.managed_config_items["elb_internal_endpoint"]
- aws_secretsmanager_secret.managed_config_items["rds_postgres_endpoint"]
- aws_secretsmanager_secret.managed_config_items["smtp_parameters"]
- aws_secretsmanager_secret.secret["gitlab_secrets_backup"]
```

The location of an exemption directory can be specified with `--exemptions-dir`.
Note that this directory will be relative to `--chdir` if that option is used.

```console
$ terra-transformer protected-apply ...
...
-------------------------------------------------------------------------
⚠️  Terraform Plan contains changes that will delete protected resources.

The following protected resources will be deleted:

- aws_secretsmanager_secret.managed_config_items["aws_elasticache_primary_endpoint_address"]
- aws_secretsmanager_secret.managed_config_items["elastic_ip"]
- aws_secretsmanager_secret.managed_config_items["elb_internal_endpoint"]
- aws_secretsmanager_secret.managed_config_items["rds_postgres_endpoint"]
- aws_secretsmanager_secret.managed_config_items["smtp_parameters"]
- aws_secretsmanager_secret.secret["gitlab_secrets_backup"]

⚠️  This may lead to loss of critical data. Manual intervention required.
-------------------------------------------------------------------------
$ ls protection-exemptions/
v14-remove-shadow-secrets.exemption.yml
$ terra-transformer protected-apply --exemptions-dir 'protection-exemptions'
...
Apply complete! Resources: 1 added, 4 changed, 7 destroyed.
```

##### Environment Substitutions

Note that environment variables will be expanded in exemption addresses in exemption files.

For example, given the following example:

```yaml
exemptions:
- module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["${TENANT_ID}-consul-1-logs"]
- module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["${TENANT_ID}-consul-2-logs"]
- module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["${TENANT_ID}-consul-3-logs"]
```

`TENANT_ID` will be replaced with the value of the environment variable `TENANT_ID`.
If the environment variable is not set, an empty string value will be used.


#### ⚠️DEPRECATED: Exemptions by Type

⚠️NOTE THAT THIS APPROACH TO EXEMPTIONS IS DEPRECATED AND WILL BE REMOVED IN FUTURE. ⚠️

Temporary exemptions can be used to bypass protections for all instances of a specific type, for a short period.

This can be done by passing the exemption via the `TERRA_PROTECT_RESOURCE_EXEMPTION` environment variable.

The structure of the exemption string is as follows:

`{timestamp},{duration}={exempted_resource_type_1},{exempted_resource_type_2},{exempted_resource_type_3}`

1. `timestamp` is an [`RFC3339`](https://datatracker.ietf.org/doc/html/rfc3339) formatted timestamp.
1. `duration` is a [Golang-style duration string](https://pkg.go.dev/time#ParseDuration). The duration may not exceed 24 hours.
1. `exempted_resource_type_1` are AWS resource types that will temporarily be allowed to be destroyed
1. If an exemption string is presented, but does not parse, the program will return an error.
1. If an exemption string is presented, but the current time is not within the timestamp + duration window, the program will return an error.

##### Examples of correct and incorrect exemptions

```shell
# correct
TERRA_PROTECT_RESOURCE_EXEMPTION=2022-05-15T00:00:00Z,2h=aws_rds_instance # correct, allows exemption for aws_rds_instance for 2h on 2022-05-15 00h00
TERRA_PROTECT_RESOURCE_EXEMPTION=2022-05-15T00:00:00Z,2h=aws_rds_instance,aws_ec2_instance # correct, allows exemption for aws_rds_instance and aws_ec2_instance for 2h on 2022-05-15 00h00

# incorrect
TERRA_PROTECT_RESOURCE_EXEMPTION=2022-05-15T00:00:00Z=aws_rds_instance # incorrect, exemption duration omitted
TERRA_PROTECT_RESOURCE_EXEMPTION=2022-05-15T00:00:00Z,48h=aws_rds_instance # incorrect, duration exceeds 24h
```

### `protected-apply`

An all-in-one auto-apply. This command was added for convenience, and it runs Terraform subsequently with `init`, `plan` and `apply` safely.

1. Performs a `terraform init`, optionally with a `--backend-config` argument.
1. Performs a `terraform plan`.
1. Generates JSON plan file from the `terraform plan`
1. Runs `terra-transform protect` to ensure that no protected resources will be destroyed. Protected resources will be loaded from the configuration file, which can be specified with `--config <file>`. Exemptions will also be handled as per `terra-transform protect`.
1. Runs `terraform apply` with the plan.

Example usage:

```console
$ # configure an exemption for `aws_ebs_volume` and `aws_db_instance`
$ export TERRA_PROTECT_RESOURCE_EXEMPTION=2022-05-15T00:00:00Z,2h=aws_db_instance,aws_ebs_volume
$ cat config.yaml
protected_types:
- aws_backup_vault
- aws_db_instance
- aws_ebs_volume
$ terra-transformer --config config.yaml auto-apply --chdir onboard --plan-file onboard.tfplan --backend-config s3backed.tfvars.json
...
```

### `undo-move`

This subcommand takes a Terraform file containing `moved` blocks as input and reverses the Terraform `moved` refactors using the current Terraform state (obtained with `terraform state list`).

The command will issue a set of `terraform state mv` commands, but it is up to the operator to issue those commands.

This may help during some development operations to undo a previous refactor.

**NOTE**: `undo-move` is an EXPERIMENTAL FEATURE. Use with care.

```console
$ terra-transformer undo-move migration-2022-08-22-regionalize-provision.tf
terraform state mv 'module.backup_global.aws_backup_vault.replica_backup_vault' 'aws_backup_vault.replica_backup_vault'
terraform state mv 'module.backup_global.aws_backup_vault_lock_configuration.replica_vault_lock_configuration' 'aws_backup_vault_lock_configuration.replica_vault_lock_configuration'
terraform state mv 'module.backup_global.aws_iam_role.backups' 'aws_iam_role.backups'
```

Given the `terraform state mv` output commands, the operator can issue these commands individually.
