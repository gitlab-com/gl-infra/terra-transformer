FROM registry.access.redhat.com/ubi9/ubi:9.5-1739751568 as certs

FROM scratch
ARG BUILD_ID

# scratch binaries stay in the root
COPY ${BUILD_ID} /terra-transformer

# Go will look for root certs here: https://go.dev/src/crypto/x509/root_linux.go
COPY --from=certs /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem /etc/pki/ca-trust/extracted/pem/

ENTRYPOINT ["/terra-transformer"]
