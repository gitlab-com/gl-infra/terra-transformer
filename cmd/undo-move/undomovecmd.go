package undomovecmd

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"

	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

var cmd = &cobra.Command{
	Use:        "undo-move",
	ArgAliases: []string{"moves.tf"},

	Short: "Generate a set of mv commands",
	Long:  `Generate a set of mv commands to undo a Terraform moved block refactor`,
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		moves, err := terraform.ParseMovedBlocks(args[0])
		if err != nil {
			return fmt.Errorf("unable to parse moves terraform: %w", err)
		}

		state, err := listTerraformState()
		if err != nil {
			return fmt.Errorf("unable to list terraform state: %w", err)
		}

		reverseMoves, err := reverseMoves(moves, state)
		if err != nil {
			return fmt.Errorf("move reverse failed: %w", err)
		}

		for _, move := range reverseMoves {
			fmt.Printf("terraform state mv '%s' '%s'\n", move.From, move.To)
		}

		return nil
	},
}

func reverseMoves(moves []terraform.Move, state []string) ([]terraform.Move, error) {
	result := []terraform.Move{}

	toLookups := make(map[string]string, len(moves))

	// Create a map of from->to
	for _, v := range moves {
		toLookups[v.To] = v.From
	}

OUTER:
	for _, item := range state {
		if strings.Contains(item, ".data.") {
			// Skip data items
			continue
		}

		// Look for a direct lookup
		originalFrom, ok := toLookups[item]
		if ok {
			result = append(result, terraform.Move{From: item, To: originalFrom})
			continue OUTER
		}

		// Now try indirect...
		// Makes this O(n2) but really not a big deal for
		// once-off tool
		for _, v := range moves {
			// Submodule Moves
			if strings.HasPrefix(item, v.To+".") {
				target := strings.TrimPrefix(item, v.To+".")
				target = v.From + "." + target

				result = append(result, terraform.Move{From: item, To: target})

				continue OUTER
			}

			// Indexed resource moves
			if strings.HasPrefix(item, v.To+"[") {
				target := strings.TrimPrefix(item, v.To)
				target = v.From + target

				result = append(result, terraform.Move{From: item, To: target})

				continue OUTER
			}
		}
	}

	return result, nil
}

func listTerraformState() ([]string, error) {
	w := &bytes.Buffer{}

	err := terraform.RunTerraformWithOutput([]string{"state", "list"}, w)
	if err != nil {
		return nil, fmt.Errorf("unable to load state: %w", err)
	}

	result := []string{}

	scanner := bufio.NewScanner(w)
	for scanner.Scan() {
		result = append(result, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, fmt.Errorf("unable to read state: %w", err)
	}

	return result, nil
}

func init() {
	root.RootCmd.AddCommand(cmd)
}
