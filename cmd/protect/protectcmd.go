package importcmd

import (
	"fmt"

	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/protect"
)

var planFile string
var types []string
var exemptionsDirectory string

var cmd = &cobra.Command{
	Use:        "protect",
	ArgAliases: []string{"address"},

	Short: "Protect resources",
	Long:  `Protects resources in Terraform from deletion.`,
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		err := protect.Apply(planFile, types, false, exemptionsDirectory)
		if err != nil {
			return fmt.Errorf("protect failed: %w", err)
		}

		return nil
	},
}

func init() {
	cmd.PersistentFlags().StringVarP(&planFile, "plan-file", "p", "", "Plan file to use")
	cmd.PersistentFlags().StringSliceVarP(&types, "type", "t", []string{}, "")
	cmd.PersistentFlags().StringVar(&exemptionsDirectory, "exemptions-dir", "",
		"Directory containing exemption files (*.exemption.yml) for protection exemptions.")

	root.RootCmd.AddCommand(cmd)
}
