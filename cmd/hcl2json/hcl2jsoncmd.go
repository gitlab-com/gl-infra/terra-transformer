package importcmd

import (
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"

	hcl2json "github.com/tmccombs/hcl2json/convert"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
)

var cmd = &cobra.Command{
	Use:   "hcl2json",
	Short: "Convert hcl into json",
	Long:  `Convert hcl into json`,
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		input, err := io.ReadAll(os.Stdin)
		if err != nil {
			return fmt.Errorf("unable to read from stdin: %w", err)
		}

		json, err := hcl2json.Bytes(input, "stdin.hcl", hcl2json.Options{})

		if err != nil {
			return fmt.Errorf("unable to parse HCL: %w", err)
		}

		fmt.Println(string(json))

		return nil
	},
}

func init() {
	root.RootCmd.AddCommand(cmd)
}
