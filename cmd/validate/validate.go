package validate

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/fatih/color"
	"github.com/mitchellh/go-wordwrap"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

var ignoreMissingFiles bool
var chdir string
var deleteDotTerraformDirectoryInNeeded bool

var sourceColor = color.New(color.FgYellow).Add(color.Underline)
var summaryColor = color.New(color.FgGreen).Add(color.Bold)
var descriptionColor = color.New(color.Faint)

var cmd = &cobra.Command{
	Use:   "validate",
	Short: "Validate a module",
	Long:  `Validate a Terraform module`,
	Args:  cobra.ExactArgs(0),
	RunE: func(cmd *cobra.Command, _ []string) error {
		p, terraformFailedErr := terraformValidate()
		if p == nil {
			return fmt.Errorf("terraform validate failed: %w", terraformFailedErr)
		}

		// If terraform is complaining about "Module Not Installed" errors,
		// we need to perform a terraform init
		if terraformFailedErr != nil && requiresReinitialization(p) {
			err := terraformInit(deleteDotTerraformDirectoryInNeeded)
			if err != nil {
				return fmt.Errorf("terraform init failed: %w", err)
			}

			p, terraformFailedErr = terraformValidate()
			if p == nil {
				return fmt.Errorf("terraform validate failed: %w", terraformFailedErr)
			}
		}

		if ignoreMissingFiles {
			p = filterMissingFiles(p)
		}

		p = recount(p)

		// Display errors
		emitOutput(p)

		if p.ErrorCount > 0 {
			os.Exit(1)
		}

		return nil
	},
}

func terraformInit(deleteDotTerraformDirectoryInNeeded bool) error {
	var args []string

	if chdir != "" {
		args = append(args, "-chdir="+chdir)
	}

	args = append(args, "init", "-backend=false", "-lockfile=readonly", "-reconfigure")

	err := terraform.RunTerraformWithOutput(args, os.Stderr)
	if err == nil {
		return nil
	}

	if !errors.Is(err, terraform.ErrNonZeroExitCode) || !deleteDotTerraformDirectoryInNeeded {
		return fmt.Errorf("terraform init failed: %w", err)
	}

	// terraform init failed, and we can delete the .terraform directory

	terraformPath := path.Join(chdir, ".terraform")

	err = os.RemoveAll(terraformPath)
	if err != nil {
		return fmt.Errorf("unable to delete %s: %w", terraformPath, err)
	}

	// Try the terraform init command again
	err = terraform.RunTerraformWithOutput(args, os.Stderr)
	if err != nil {
		return fmt.Errorf("terraform init failed, even after deleting the .terraform directory: %w", err)
	}

	return nil
}

func terraformValidate() (*tfjson.ValidateOutput, error) {
	var args []string

	if chdir != "" {
		args = append(args, "-chdir="+chdir)
	}

	args = append(args, "validate", "-json")

	var terraformFailedErr error

	out := &bytes.Buffer{}

	err := terraform.RunTerraformWithOutput(args, out)
	if err != nil {
		terraformFailedErr = fmt.Errorf("terraform failed: %w", err)

		if !errors.Is(err, terraform.ErrNonZeroExitCode) {
			return nil, terraformFailedErr
		}
	}

	p := &tfjson.ValidateOutput{}

	err = p.UnmarshalJSON(out.Bytes())
	if err != nil {
		if terraformFailedErr != nil {
			return nil, terraformFailedErr
		}

		return nil, fmt.Errorf("failed to unmarshall validation output: %w", err)
	}

	return p, terraformFailedErr
}

func filterMissingFiles(p *tfjson.ValidateOutput) *tfjson.ValidateOutput {
	filteredDiagnostics := make([]tfjson.Diagnostic, 0)

	for _, diagnostic := range p.Diagnostics {
		if !isInvalidPathError(diagnostic) {
			filteredDiagnostics = append(filteredDiagnostics, diagnostic)
		}
	}

	p.Diagnostics = filteredDiagnostics

	return p
}

func recount(p *tfjson.ValidateOutput) *tfjson.ValidateOutput {
	e := 0
	w := 0

	//nolint:exhaustive
	for _, diagnostic := range p.Diagnostics {
		switch diagnostic.Severity {
		case tfjson.DiagnosticSeverityError:
			e = e + 1
		case tfjson.DiagnosticSeverityWarning:
			w = w + 1
		}
	}

	p.ErrorCount = e
	p.WarningCount = w

	return p
}

func isInvalidPathError(diagnostic tfjson.Diagnostic) bool {
	return diagnostic.Summary == "Invalid function argument" && strings.Contains(diagnostic.Detail, "no file exists at ")
}

// Detect "Module not installed" errors, which indicate that `terraform init` needs to be rerun.
func requiresReinitialization(p *tfjson.ValidateOutput) bool {
	for _, diagnostic := range p.Diagnostics {
		if diagnostic.Severity == tfjson.DiagnosticSeverityError && diagnostic.Summary == "Module not installed" {
			return true
		}

		// "registry.terraform.io/hashicorp/google: there is no package for registry.terraform.io/hashicorp/google"
		if diagnostic.Severity == tfjson.DiagnosticSeverityError && strings.Contains(diagnostic.Summary, "there is no package for ") {
			return true
		}

		if diagnostic.Severity == tfjson.DiagnosticSeverityError && strings.Contains(diagnostic.Summary, "error loading plugin path") {
			return true
		}
	}

	return false
}

func emitOutput(p *tfjson.ValidateOutput) {
	for i, diagnostic := range p.Diagnostics {
		c, sevString := severityName(diagnostic.Severity)

		c.Add(color.Bold).Printf("Issue #%d: %s : %s\n", i+1, sevString, diagnostic.Summary)

		if diagnostic.Detail != "" {
			descriptionColor.Println(wordwrap.WrapString(diagnostic.Detail, 100)) //nolint:errcheck
		}

		if diagnostic.Range != nil && diagnostic.Range.Filename != "" {
			relativePath, err := filepath.Rel(".", path.Join(chdir, diagnostic.Range.Filename))
			if err != nil {
				relativePath = diagnostic.Range.Filename
			}

			fmt.Printf("Source: %s\n", sourceColor.Sprintf("%s:%d", relativePath, diagnostic.Range.Start.Line))
		}

		fmt.Println()
	}

	moduleName := chdir
	if moduleName == "" {
		moduleName = "."
	}

	summaryColor.Printf("Summary for module %s: %d error(s), %d warning(s)\n", moduleName, p.ErrorCount, p.WarningCount)
}

func severityName(diagnosticSeverity tfjson.DiagnosticSeverity) (*color.Color, string) {
	//nolint:exhaustive
	switch diagnosticSeverity {
	case tfjson.DiagnosticSeverityError:
		return color.New(color.FgRed), "🛑 Error"

	case tfjson.DiagnosticSeverityWarning:
		return color.New(color.FgYellow), "⚠️ Warning"

	default:
		return color.New(color.FgWhite), "‼️ Unknown"
	}
}

func init() {
	cmd.PersistentFlags().BoolVarP(&ignoreMissingFiles, "ignore-missing-files", "", false, "Ignore validation errors due to missing files")
	cmd.PersistentFlags().StringVarP(&chdir, "chdir", "C", "", "Change directory")
	cmd.PersistentFlags().BoolVarP(
		&deleteDotTerraformDirectoryInNeeded,
		"delete-dot-terraform-if-required", "", false,
		"If terraform init fails, allow the deletion of the .terraform directory to clean any corrupted state")

	root.RootCmd.AddCommand(cmd)
}
