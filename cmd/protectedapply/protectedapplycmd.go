package autoapplycommand

import (
	"fmt"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"gitlab.com/gitlab-com/gl-infra/terra-transformer/pkg/retry-cmd"

	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/protect"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

var chdir string
var backendConfig string
var planFileJSON string
var dryRun bool
var exemptionsDirectory string
var targets []string
var replaces []string
var parallelism int
var varFiles []string
var jsonOutput bool
var maxRetries uint
var initialRetryDelay time.Duration

var cmd = &cobra.Command{
	Use:        "protected-apply",
	ArgAliases: []string{"address"},

	Short: "All-in-one auto-apply for terraform apply, with protection",
	Long:  `All-in-one auto-apply for terraform apply, with protection`,
	Args:  cobra.MinimumNArgs(0),
	RunE: retry.Cmd(func(cmd *cobra.Command, args []string) error {
		f, err := os.CreateTemp("", "terraform-plan")
		if err != nil {
			return fmt.Errorf("failed to create temporary plan file: %w", err)
		}
		defer os.Remove(f.Name()) // clean up
		planFile := f.Name()

		if planFileJSON == "" {
			planFileJSON = planFile + ".json"
			defer os.Remove(planFileJSON) // clean up
		}

		err = initTerraform()
		if err != nil {
			return err
		}

		err = planTerraform(planFile, targets, replaces)
		if err != nil {
			return err
		}

		err = showTerraformPlan(planFile, planFileJSON)
		if err != nil {
			return err
		}

		if exemptionsDirectory != "" && chdir != "" {
			exemptionsDirectory = path.Join(chdir, exemptionsDirectory)
		}

		// Run protection checks
		err = protect.Apply(planFileJSON, nil, true, exemptionsDirectory)
		if err != nil {
			return fmt.Errorf("protect rejected plan: %w", err)
		}

		err = applyTerraformPlan(planFile, parallelism)
		if err != nil {
			return err
		}

		return nil
	},
		// maxRetries and initialRetryDelay need to be pointers rather than value, because at the time that
		// RunE is being setup as a &Command (ie, during `init`) the command-line
		// argument has not yet been parsed.
		// It's only parsed later, hence the &maxRetries
		retry.WithMaxRetries(&maxRetries),
		retry.WithInitialDelay(&initialRetryDelay),
	),
}

func getArgs(tfCommand string) []string {
	args := []string{}

	if chdir != "" {
		args = append(args, "-chdir="+chdir)
	}

	args = append(args, tfCommand)

	if jsonOutput {
		args = append(args, "-json")
	}

	return args
}

// terraform -chdir="$chdir" init -backend-config=s3backend.tfvars.
func initTerraform() error {
	args := getArgs("init")

	if backendConfig != "" {
		args = append(args, "-backend-config="+backendConfig)
	}

	err := terraform.RunTerraform(args)
	if err != nil {
		return fmt.Errorf("terraform failed: %w", err)
	}

	return nil
}

// terraform -chdir="$chdir" plan -out="$plan_file".
func planTerraform(planFile string, targets []string, replaces []string) error {
	args := getArgs("plan")
	args = append(args, "-out="+planFile)

	for _, v := range targets {
		args = append(args, "-target="+v)
	}

	for _, v := range replaces {
		args = append(args, "-replace="+v)
	}

	for _, v := range varFiles {
		args = append(args, "-var-file="+v)
	}

	err := terraform.RunTerraform(args)
	if err != nil {
		return fmt.Errorf("terraform failed: %w", err)
	}

	return nil
}

// terraform -chdir="$chdir" show --json "$plan_file" >"$plan_file_json".
func showTerraformPlan(planFile string, planFileJSON string) error {
	args := getArgs("show")

	// getArgs will already add `-json` if arg is globally configured..
	if !jsonOutput {
		args = append(args, "-json")
	}

	args = append(args, planFile)

	err := terraform.RunTerraformWriteOutputToFile(args, planFileJSON)
	if err != nil {
		return fmt.Errorf("terraform failed: %w", err)
	}

	return nil
}

// terraform -chdir="$chdir" apply -auto-approve -input=false "$plan_file".
func applyTerraformPlan(planFile string, parallelism int) error {
	args := getArgs("apply")
	args = append(args, "-auto-approve", "-input=false", planFile)

	if parallelism != 0 {
		args = append(args, "-parallelism="+strconv.Itoa(parallelism))
	}

	if dryRun {
		log.Printf("dry-run: skipping terraform %s", strings.Join(args, ""))
		return nil
	}

	err := terraform.RunTerraform(args)
	if err != nil {
		return fmt.Errorf("terraform failed: %w", err)
	}

	return nil
}

func init() {
	cmd.PersistentFlags().StringVarP(&planFileJSON, "plan-file-json", "p", "", "JSON plan-file to emit")
	cmd.PersistentFlags().BoolVarP(&dryRun, "dry-run", "D", false, "Run in dry-run mode: do not apply")
	cmd.PersistentFlags().StringVar(&chdir, "chdir", "", "Terraform chdir argument")
	cmd.PersistentFlags().StringVar(&backendConfig, "backend-config", "", "Terraform backend-config argument for init")
	cmd.PersistentFlags().StringVar(&exemptionsDirectory, "exemptions-dir", "",
		"Directory containing exemption files (*.exemption.yml) for protection exemptions.")
	cmd.PersistentFlags().StringArrayVar(&targets, "target", []string{},
		"Terraform targets to apply. Passed through via the Terraform -target argument.")
	cmd.PersistentFlags().StringArrayVar(&replaces, "replace", []string{},
		"Terraform resources to replace in apply. Passed through via the Terraform -replace argument.")
	cmd.PersistentFlags().IntVar(&parallelism, "parallelism", 0,
		"Limit the number of parallel resource operations. Corresponds to the Terraform -parallelism argument.")
	cmd.PersistentFlags().StringSliceVar(&varFiles, "var-file", []string{},
		"Pass a var-file argument to terraform.")
	cmd.PersistentFlags().BoolVarP(&jsonOutput, "json", "", false, "Run terraform with json output")

	cmd.PersistentFlags().UintVarP(&maxRetries, "max-retries", "R", 1, "Maximum number of times to retry the operation")
	cmd.PersistentFlags().DurationVar(&initialRetryDelay, "retry-delay", 30*time.Second, "The initial retry delay. "+
		"This will increase exponentially with each backoff to a maximum duration of 10 times the initial duration. "+
		"The jitter on each invocation will be set to 1/6th of the initial retry delay.")

	root.RootCmd.AddCommand(cmd)
}
