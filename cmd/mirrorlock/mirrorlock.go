package mirrorlock

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	terraform "gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
	emptymodule "gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform/emptymodule"
)

var cmd = &cobra.Command{
	Use:   "mirrorlock lock_file mirror_directory",
	Short: "EXPERIMENTAL: Mirror the providers given a lockfile",
	Long:  `EXPERIMENTAL: Mirror the providers given a lockfile`,
	Args:  cobra.ExactArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {
		lockfile := args[0]
		mirrorDirectory := args[1]

		tmpDir, err := os.MkdirTemp("", "terra-transformer-mirrorlock")
		if err != nil {
			return fmt.Errorf("failed to make tempdir: %w", err)
		}

		defer os.RemoveAll(tmpDir)

		err = emptymodule.CreateModule(tmpDir, lockfile)
		if err != nil {
			return fmt.Errorf("failed to create empty module: %w", err)
		}

		mirrorDirectoryAbs, err := filepath.Abs(mirrorDirectory)
		if err != nil {
			return fmt.Errorf("failed to resolve mirror directory: %w", err)
		}

		tfArgs := []string{
			fmt.Sprintf("-chdir=%s", tmpDir),
			"providers",
			"mirror",
			mirrorDirectoryAbs,
		}

		err = terraform.RunTerraformWithOutput(tfArgs, os.Stdout)
		if err != nil {
			return fmt.Errorf("terraform providers mirror failed: %w", err)
		}

		err = cleanupUnneededFiles(mirrorDirectory)
		if err != nil {
			return fmt.Errorf("cleanup failed: %w", err)
		}

		return nil
	},
}

func cleanupUnneededFiles(mirrorDirectory string) error {
	err := filepath.Walk(mirrorDirectory,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !info.IsDir() {
				base := filepath.Base(path)

				if !strings.HasPrefix(base, "terraform-provider-") {
					err = os.Remove(path)
					if err != nil {
						return fmt.Errorf("failed to remove file %s: %w", path, err)
					}
				}
			}

			return nil
		})
	if err != nil {
		return fmt.Errorf("cleanup failed: %w", err)
	}

	return nil
}

func init() {
	root.RootCmd.AddCommand(cmd)
}
