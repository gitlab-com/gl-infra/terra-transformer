package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var config string

// RootCmd represents the root command.
var RootCmd = &cobra.Command{
	Use:          "terra-transformer",
	Short:        "Terra-Transformer",
	Long:         `A tool for importing resources into Terraform`,
	SilenceUsage: true,
}

// Execute is the main entry point into the cobra application.
func Execute() {
	err := RootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&config, "config", "", "Configure YAML configuration file")
}

func initConfig() {
	viper.SetDefault("protected_types", []string{})

	if config == "" {
		return
	}

	viper.SetConfigType("yaml")
	viper.SetConfigFile(config)

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}
