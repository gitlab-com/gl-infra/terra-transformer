package importcmd

import (
	"fmt"

	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

var planFile string
var dryRun bool

var cmd = &cobra.Command{
	Use:        "rm",
	ArgAliases: []string{"address"},

	Short: "Removes a resource from state",
	Long:  `Removes a resource from state`,
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		p, err := terraform.LoadPlan(planFile)
		if err != nil {
			return fmt.Errorf("unable to load plan: %w", err)
		}

		for _, addr := range args {
			err = terraform.Remove(p, addr, dryRun)
			if err != nil {
				return fmt.Errorf("terraform import failed: %w", err)
			}
		}

		return nil
	},
}

func init() {
	cmd.PersistentFlags().StringVarP(&planFile, "plan-file", "p", "", "Plan file to use")
	cmd.PersistentFlags().BoolVarP(&dryRun, "dry-run", "D", false, "Run in dry-run mode: output the terraform command, but do not run it")
	root.RootCmd.AddCommand(cmd)
}
