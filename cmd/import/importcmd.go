package importcmd

import (
	"fmt"
	"os"

	"github.com/kballard/go-shellquote"
	"github.com/spf13/cobra"

	root "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

var planFile string
var importArgs string
var awsProfile string
var dryRun bool
var skipUnknown bool

var cmd = &cobra.Command{
	Use:        "import",
	ArgAliases: []string{"address"},

	Short: "Import resources into Terraform",
	Long:  `Imports resources into Terraform. When possible, favour the new import block`,
	Args:  cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		p, err := terraform.LoadPlan(planFile)
		if err != nil {
			return fmt.Errorf("unable to load plan: %w", err)
		}

		extraArguments, err := shellquote.Split(importArgs)
		if err != nil {
			return fmt.Errorf("unable to parse extra terraform import arguments: %w", err)
		}

		for _, addr := range args {
			err = terraform.Import(p, addr, extraArguments, awsProfile, dryRun, skipUnknown)
			if err != nil {
				return fmt.Errorf("terraform import failed: %w", err)
			}
		}

		return nil
	},
}

func init() {
	cmd.PersistentFlags().StringVarP(&planFile, "plan-file", "p", "", "Plan file to use")
	cmd.PersistentFlags().StringVarP(&importArgs, "import-arguments", "a", "", "Additional arguments to pass to `terraform import`")
	cmd.PersistentFlags().StringVarP(&awsProfile, "aws-profile", "P", os.Getenv("AWS_PROFILE"), "AWS profile used for imports. Defaults to `AWS_PROFILE`.")
	cmd.PersistentFlags().BoolVarP(&dryRun, "dry-run", "D", false, "Run in dry-run mode: output the terraform command, but do not run it")
	cmd.PersistentFlags().BoolVar(&skipUnknown, "skip-unknown", false, "Skip importing terraform resources with unknown attributes")
	root.RootCmd.AddCommand(cmd)
}
