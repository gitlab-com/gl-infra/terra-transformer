package main

import (
	"gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/hcl2json"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/import"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/mirrorlock"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/protect"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/protectedapply"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/rm"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/undo-move"
	_ "gitlab.com/gitlab-com/gl-infra/terra-transformer/cmd/validate"
)

func main() {
	cmd.Execute()
}
