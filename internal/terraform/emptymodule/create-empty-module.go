package emptymodule

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path"
)

// CreateModule will an empty module directory, given a lockfile.
func CreateModule(dir string, lockfile string) (errOut error) {
	mainModule := path.Join(dir, "main.tf")

	f, err := os.Create(mainModule)
	if err != nil {
		return fmt.Errorf("failed to create file: %w", err)
	}

	defer func() {
		// Report the error, if any, from Close, but do so
		// only if there isn't already an outgoing error.
		if c := f.Close(); errOut == nil {
			errOut = c
		}
	}()

	w := bufio.NewWriter(f)

	err = convertLockFileToEmptyModule(lockfile, w)
	if err != nil {
		return fmt.Errorf("failed to convert lockfile: %w", err)
	}

	err = w.Flush()
	if err != nil {
		return fmt.Errorf("failed to flush: %w", err)
	}

	err = copyFile(lockfile, path.Join(dir, ".terraform.lock.hcl"))
	if err != nil {
		return fmt.Errorf("failed to copy lockfile: %w", err)
	}

	return nil
}

// Copy copies the contents of the file at srcpath to a regular file
// at dstpath. If the file named by dstpath already exists, it is
// truncated. The function does not copy the file mode, file
// permission bits, or file attributes.
func copyFile(srcpath, dstpath string) (err2 error) {
	r, err := os.Open(srcpath)
	if err != nil {
		return fmt.Errorf("file open failed: %w", err)
	}
	defer r.Close() // ignore error: file was opened read-only.

	w, err := os.Create(dstpath)
	if err != nil {
		return fmt.Errorf("file create failed: %w", err)
	}

	defer func() {
		// Report the error, if any, from Close, but do so
		// only if there isn't already an outgoing error.
		if c := w.Close(); err2 == nil {
			err2 = c
		}
	}()

	_, err = io.Copy(w, r)
	if err != nil {
		return fmt.Errorf("file copy failed: %w", err)
	}

	return nil
}
