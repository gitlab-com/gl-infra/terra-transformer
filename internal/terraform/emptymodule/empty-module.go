package emptymodule

import (
	"fmt"
	"io"
	"os"

	hcl "github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsyntax"
	hcl2json "github.com/tmccombs/hcl2json/convert"
)

//var errConversionFailed = errors.New("conversion failed")

// convertLockFileToEmptyModule will convert a lock file into an empty module, suitable for use with
// the `terraform providers mirror` command.
func convertLockFileToEmptyModule(lockfilePath string, writer io.Writer) error {
	tfFileContents, err := os.ReadFile(lockfilePath)
	if err != nil {
		return fmt.Errorf("unable to read from stdin: %w", err)
	}

	file, diags := hclsyntax.ParseConfig(tfFileContents, lockfilePath, hcl.Pos{Line: 1, Column: 1})
	if diags.HasErrors() {
		return fmt.Errorf("unable to parse HCL: %w", diags)
	}

	j, err := hcl2json.ConvertFile(file, hcl2json.Options{Simplify: false})
	if err != nil {
		return fmt.Errorf("unable to read lockfile: %w", err)
	}

	tmpl, err := getTemplate()
	if err != nil {
		return fmt.Errorf("unable to parse template: %w", err)
	}

	err = tmpl.Execute(writer, j)
	if err != nil {
		return fmt.Errorf("unable to execute template: %w", err)
	}

	return nil
}
