package emptymodule

import (
	"bufio"
	"bytes"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestConvertLockFileToEmptyModule(t *testing.T) {
	tests := []struct {
		name        string
		fixturePath string
		wantErr     bool
	}{
		{
			name:        "first",
			fixturePath: "./fixtures/fixture1/",
			wantErr:     false,
		},
	}

	t.Parallel()

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			b := bytes.Buffer{}
			writer := bufio.NewWriter(&b)

			err := convertLockFileToEmptyModule(path.Join(tt.fixturePath, ".terraform.lock.hcl"), writer)
			if tt.wantErr {
				require.Error(t, err, "expected an error")
			} else {
				require.NoError(t, err, "expected no error")
				writer.Flush()

				expected, err := os.ReadFile(path.Join(tt.fixturePath, "expected.tf"))
				require.NoError(t, err, "unable to read fixture's expected.tf file")

				require.Equal(t, strings.TrimSpace(string(expected)), strings.TrimSpace(b.String()))
			}
		})
	}
}
