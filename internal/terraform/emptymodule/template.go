package emptymodule

import (
	"fmt"
	"strings"
	"text/template"
)

var templateSource = strings.TrimSpace(`
terraform {
	required_providers {
	{{- range $key, $value := .provider }}
		{{ providerShortName $key }} = {
			source  = "{{ $key }}"
			version = "{{ index (index $value 0) "version" }}"
		}{{ end }}
	}
}
`)

func providerShortName(providerName string) string {
	a := strings.Split(providerName, "/")
	return a[len(a)-1]
}

func getTemplate() (*template.Template, error) {
	tmpl, err := template.New("empty-provider").Funcs(template.FuncMap{
		"providerShortName": providerShortName,
	}).Parse(templateSource)

	if err != nil {
		return nil, fmt.Errorf("unable to parse template: %w", err)
	}

	return tmpl, nil
}
