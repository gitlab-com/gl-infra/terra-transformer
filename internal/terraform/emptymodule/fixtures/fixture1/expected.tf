terraform {
	required_providers {
		archive = {
			source  = "registry.terraform.io/hashicorp/archive"
			version = "2.7.0"
		}
		aws = {
			source  = "registry.terraform.io/hashicorp/aws"
			version = "5.90.0"
		}
		external = {
			source  = "registry.terraform.io/hashicorp/external"
			version = "2.3.4"
		}
		null = {
			source  = "registry.terraform.io/hashicorp/null"
			version = "3.2.3"
		}
		time = {
			source  = "registry.terraform.io/hashicorp/time"
			version = "0.13.0"
		}
		tls = {
			source  = "registry.terraform.io/hashicorp/tls"
			version = "4.0.6"
		}
	}
}
