package importers

import (
	"context"
	"errors"
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"
)

type googleImportFunc func(ctx context.Context, plannedValues map[string]interface{}, plannedResource *tfjson.StateResource) (string, error)

var ErrUnableToResolveResourceID = errors.New("unable to resolve resource ID from plan attributes")

func googleImporter(f googleImportFunc) Importer {
	return func(plannedResource *tfjson.StateResource, _ string) (string, error) {
		identifier, err := f(context.TODO(), plannedResource.AttributeValues, plannedResource)
		if err != nil {
			return "", fmt.Errorf("failed to query for resource: %w", err)
		}

		return identifier, nil
	}
}

func formatPlannedAttributes(format string, plannedValues map[string]interface{}, attributes ...string) (string, error) {
	values := make([]interface{}, len(attributes))

	for i, a := range attributes {
		value, ok := plannedValues[a]
		if !ok {
			return "", fmt.Errorf("unable to resolve ID. Attribute '%s' is not available: %w", a, ErrUnableToResolveResourceID)
		}

		values[i] = value
	}

	return fmt.Sprintf(format, values...), nil
}
