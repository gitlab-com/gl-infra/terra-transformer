package importers

import (
	"fmt"
	"log"
	"sort"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
)

var awsKMSKeyImporter = awsImporter("description", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := kms.New(sess)

	var foundResources []*kms.DescribeKeyOutput

	err := svc.ListKeysPages(&kms.ListKeysInput{}, func(output *kms.ListKeysOutput, lastPage bool) bool {
		for _, c := range output.Keys {
			d, err := svc.DescribeKey(&kms.DescribeKeyInput{KeyId: c.KeyId})
			if err != nil {
				log.Printf("failed to describe key: %s: %v", *c.KeyId, err)
				continue
			}

			if *d.KeyMetadata.Description == plannedValue && *d.KeyMetadata.KeyState == "Enabled" {
				foundResources = append(foundResources, d)
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if len(foundResources) == 0 {
		return "", nil
	}

	sort.SliceStable(foundResources, func(i, j int) bool {
		return foundResources[i].KeyMetadata.CreationDate.Before(*foundResources[j].KeyMetadata.CreationDate)
	})

	// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key#import
	// KMS Keys can be imported using the id, e.g.,
	// terraform import aws_kms_key.a 1234abcd-12ab-34cd-56ef-1234567890ab
	return *foundResources[0].KeyMetadata.KeyId, nil
})
