package importers

import (
	"fmt"
	"strings"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/route53"
)

var awsRoute53ZoneImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := route53.New(sess)

	var foundResource *route53.HostedZone

	err := svc.ListHostedZonesPages(&route53.ListHostedZonesInput{}, func(output *route53.ListHostedZonesOutput, lastPage bool) bool {
		for _, c := range output.HostedZones {
			if *c.Name == (plannedValue + ".") {
				foundResource = c
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone#import
		// Route53 Zones can be imported using the zone id, e.g.,
		// terraform import aws_route53_zone.myzone Z1D633PJN98FT9
		parts := strings.Split(*foundResource.Id, "/")
		return parts[2], nil
	}

	return "", nil
})
