package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
)

type awsImportFunc func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error)

func awsImporter(plannedValueKey string, f awsImportFunc) Importer {
	return func(plannedResource *tfjson.StateResource, awsProfile string) (string, error) {
		plannedValueIntf := plannedResource.AttributeValues[plannedValueKey]

		plannedValue, ok := plannedValueIntf.(string)
		if !ok {
			return "", fmt.Errorf("planned resource does not have a `%s` attribute: %w", plannedValueKey, ErrUnableToResolveResourceID)
		}

		sess, err := session.NewSessionWithOptions(session.Options{
			Config: aws.Config{
				CredentialsChainVerboseErrors: aws.Bool(true),
			},
			Profile: awsProfile,
		})

		if err != nil {
			return "", fmt.Errorf("unable to establish AWS session: %w", err)
		}

		identifier, err := f(sess, plannedValue, plannedResource)
		if err != nil {
			return "", fmt.Errorf("failed to query for resource: %w", err)
		}

		return identifier, nil
	}
}
