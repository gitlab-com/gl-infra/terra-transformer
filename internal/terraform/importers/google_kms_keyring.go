package importers

import (
	"context"
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	kms "cloud.google.com/go/kms/apiv1"
	"cloud.google.com/go/kms/apiv1/kmspb"
)

var googleKMSKeyRingImporter = googleImporter(func(ctx context.Context,
	plannedValues map[string]interface{}, plannedResource *tfjson.StateResource) (string, error) {
	selfID, err := formatPlannedAttributes("projects/%s/locations/%s/keyRings/%s", plannedValues, "project", "location", "name")
	if err != nil {
		return "", err
	}

	client, err := kms.NewKeyManagementClient(ctx)
	if err != nil {
		return "", fmt.Errorf("unable to create KMS client: %w", err)
	}

	_, err = client.GetKeyRing(ctx, &kmspb.GetKeyRingRequest{
		Name: selfID,
	})

	if err != nil {
		// Getting an error while looking up the object is expected as this indicates
		// it does not exist in the cloud and does not need to be imported.
		return "", nil
	}

	return selfID, nil
})
