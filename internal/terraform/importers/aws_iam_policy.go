package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var awsIAMPolicyImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := iam.New(sess)

	var foundResource *iam.Policy
	err := svc.ListPoliciesPages(&iam.ListPoliciesInput{}, func(output *iam.ListPoliciesOutput, lastPage bool) bool {
		for _, p := range output.Policies {
			if *p.PolicyName == plannedValue {
				foundResource = p
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy#import
		// IAM Policies can be imported using the arn, e.g.,
		// terraform import aws_iam_policy.administrator arn:aws:iam::123456789012:policy/UsersManageOwnCredentials
		return *foundResource.Arn, nil
	}

	return "", nil
})
