package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var awsIAMRoleImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	// Create a IAM service client.
	svc := iam.New(sess)

	var foundResource *iam.Role
	err := svc.ListRolesPages(&iam.ListRolesInput{}, func(output *iam.ListRolesOutput, lastPage bool) bool {
		for _, p := range output.Roles {
			if *p.RoleName == plannedValue {
				foundResource = p
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role#import
		// IAM Roles can be imported using the name, e.g.,
		// terraform import aws_iam_role.developer developer_name

		return *foundResource.RoleName, nil
	}

	return "", nil
})
