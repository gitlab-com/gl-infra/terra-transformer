package importers

import "errors"

var ErrUnknownAttribute = errors.New("unknown attribute")
