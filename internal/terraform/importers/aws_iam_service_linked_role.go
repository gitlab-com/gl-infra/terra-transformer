package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var awsIAMServiceLinkedRoleImporter = awsImporter("aws_service_name",
	func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
		// Create a IAM service client.
		svc := iam.New(sess)

		prefix := "/aws-service-role/" + plannedValue
		var foundResource *iam.Role
		err := svc.ListRolesPages(&iam.ListRolesInput{PathPrefix: &prefix}, func(output *iam.ListRolesOutput, lastPage bool) bool {
			for _, p := range output.Roles {
				foundResource = p
				return false
			}

			return true
		})

		if err != nil {
			return "", fmt.Errorf("resource query failed: %w", err)
		}

		if foundResource != nil {
			// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role#import
			// IAM service-linked roles can be imported using role ARN, e.g.,
			// terraform import aws_iam_service_linked_role.elasticbeanstalk \
			// arn:aws:iam::123456789012:role/aws-service-role/elasticbeanstalk.amazonaws.com/AWSServiceRoleForElasticBeanstalk

			return *foundResource.Arn, nil
		}

		return "", nil
	})
