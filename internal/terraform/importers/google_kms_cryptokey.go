package importers

import (
	"context"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"google.golang.org/protobuf/types/known/fieldmaskpb"

	kms "cloud.google.com/go/kms/apiv1"
	"cloud.google.com/go/kms/apiv1/kmspb"
	tfjson "github.com/hashicorp/terraform-json"
	retry "github.com/sethvargo/go-retry"
)

var errInvalidResource = errors.New("invalid resource")

var googleKMSKeyImporter = googleImporter(func(ctx context.Context,
	plannedValues map[string]interface{}, plannedResource *tfjson.StateResource) (string, error) {

	selfID, err := formatPlannedAttributes("%s/cryptoKeys/%s", plannedValues, "key_ring", "name")
	if err != nil {
		return "", err
	}

	client, err := kms.NewKeyManagementClient(ctx)
	if err != nil {
		return "", fmt.Errorf("unable to create KMS client: %w", err)
	}

	cryptokey, err := client.GetCryptoKey(ctx, &kmspb.GetCryptoKeyRequest{
		Name: selfID,
	})

	if err != nil {
		// Getting an error while looking up the object is expected as this indicates
		// it does not exist in the cloud and does not need to be imported.
		return "", nil
	}

	err = ensureGoogleCryptoKeyState(ctx, client, cryptokey)
	if err != nil {
		log.Printf("import: error: failed to revive crypto key %s: %s", selfID, err)

		return "", err
	}

	return selfID, nil
})

// ensureGoogleCryptoKeyState ensures that the crypto key is in a usable state.
// For DESTROY_SCHEDULED it will restore the key and then enable it.
// For DISABLED it will enable the key.
// For most other states, it will throw an error.
func ensureGoogleCryptoKeyState(ctx context.Context, client *kms.KeyManagementClient, cryptokey *kmspb.CryptoKey) error {
	switch cryptokey.GetPrimary().GetState() { //nolint:exhaustive
	case kmspb.CryptoKeyVersion_PENDING_GENERATION:
		// Do nothing and allow import
		return nil

	case kmspb.CryptoKeyVersion_ENABLED:
		// Do nothing and allow import
		return nil

	case kmspb.CryptoKeyVersion_DISABLED:
		// Enable the key
		return updatePrimaryCryptoKeyState(ctx, client, cryptokey, kmspb.CryptoKeyVersion_ENABLED)

	case kmspb.CryptoKeyVersion_DESTROY_SCHEDULED:
		err := restorePrimaryCryptoKey(ctx, client, cryptokey)
		if err != nil {
			return err
		}

		return updatePrimaryCryptoKeyState(ctx, client, cryptokey, kmspb.CryptoKeyVersion_ENABLED)
	case kmspb.CryptoKeyVersion_DESTROYED:
		err := createNewKeyVersion(ctx, client, cryptokey)
		if err != nil {
			return err
		}

		return nil
	default:
		return fmt.Errorf("cryptoKey %s is in state %s and cannot be imported: %w", cryptokey.GetName(), cryptokey.GetPrimary().GetState(), errInvalidResource)
	}
}

func restorePrimaryCryptoKey(ctx context.Context, client *kms.KeyManagementClient, cryptokey *kmspb.CryptoKey) error {
	log.Printf("import: restoring deleted cryptoKey version %s ", cryptokey.GetPrimary().GetName())

	req := &kmspb.RestoreCryptoKeyVersionRequest{
		Name: cryptokey.GetPrimary().GetName(),
	}

	_, err := client.RestoreCryptoKeyVersion(ctx, req)
	if err != nil {
		return fmt.Errorf("failed to restore key version: %w", err)
	}

	err = waitForKeyCondition(ctx, client, cryptokey, func(key *kmspb.CryptoKey) bool {
		if key.GetPrimary().GetState() == kmspb.CryptoKeyVersion_ENABLED || key.GetPrimary().GetState() == kmspb.CryptoKeyVersion_DISABLED {
			return true
		}

		log.Printf("import: waiting for %s to be restored. Current state is %s ", cryptokey.GetPrimary().GetName(), cryptokey.GetPrimary().GetState())

		return false
	})

	if err != nil {
		return fmt.Errorf("import: failed to restore cryptoKey version, giving up: %w", err)
	}

	return nil
}

func updatePrimaryCryptoKeyState(ctx context.Context, client *kms.KeyManagementClient,
	cryptokey *kmspb.CryptoKey, state kmspb.CryptoKeyVersion_CryptoKeyVersionState) error {
	log.Printf("import: updating cryptoKey version %s state to %s", cryptokey.GetPrimary().GetName(), state)

	req := &kmspb.UpdateCryptoKeyVersionRequest{
		CryptoKeyVersion: &kmspb.CryptoKeyVersion{
			Name:  cryptokey.GetPrimary().GetName(),
			State: state,
		},
		UpdateMask: &fieldmaskpb.FieldMask{
			Paths: []string{"state"},
		},
	}

	_, err := client.UpdateCryptoKeyVersion(ctx, req)
	if err != nil {
		return fmt.Errorf("failed to update key version: %w", err)
	}

	err = waitForKeyCondition(ctx, client, cryptokey, func(key *kmspb.CryptoKey) bool {
		if key.GetPrimary().GetState() == state {
			return true
		}

		log.Printf("import: waiting for %s to transition to %s. "+
			"Current state is %s ", cryptokey.GetPrimary().GetName(), state, cryptokey.GetPrimary().GetState())

		return false
	})

	if err != nil {
		return fmt.Errorf("import: failed to update cryptoKey version, giving up: %w", err)
	}

	return nil
}

func createNewKeyVersion(ctx context.Context, client *kms.KeyManagementClient,
	cryptokey *kmspb.CryptoKey) error {
	log.Printf("import: %s's primary version found in the DESTROYED state; "+
		"creating a new version, if this is unexpected it may require manual intervention "+
		"to cleanup now inaccessible resources encrypted with %s",
		cryptokey.GetName(),
		cryptokey.GetPrimary().GetName())

	createReq := &kmspb.CreateCryptoKeyVersionRequest{
		Parent:           cryptokey.GetName(),
		CryptoKeyVersion: &kmspb.CryptoKeyVersion{},
	}

	keyVersion, err := client.CreateCryptoKeyVersion(ctx, createReq)
	if err != nil {
		return fmt.Errorf("failed to create a new key version: %w", err)
	}

	splitKeyVersion := strings.Split(keyVersion.GetName(), "/")
	keyVersionID := splitKeyVersion[len(splitKeyVersion)-1]

	updatePrimaryReq := &kmspb.UpdateCryptoKeyPrimaryVersionRequest{
		Name:               cryptokey.GetName(),
		CryptoKeyVersionId: keyVersionID,
	}

	_, err = client.UpdateCryptoKeyPrimaryVersion(ctx, updatePrimaryReq)
	if err != nil {
		return fmt.Errorf("failed to update the primary version: %w", err)
	}

	err = waitForKeyCondition(ctx, client, cryptokey, func(key *kmspb.CryptoKey) bool {
		if key.GetPrimary().GetState() == kmspb.CryptoKeyVersion_ENABLED {
			return true
		}

		log.Printf("import: waiting for %s's primary version to transition to %s. "+
			"Current state is %s ",
			cryptokey.GetName(),
			kmspb.CryptoKeyVersion_ENABLED,
			cryptokey.GetPrimary().GetState())

		return false
	})

	if err != nil {
		return fmt.Errorf("import: failed to create a new cryptoKey version, giving up: %w", err)
	}

	return nil
}

// waitForKeyCondition waits for a key to enter specific state, or times out after 5 minutes.
func waitForKeyCondition(ctx context.Context, client *kms.KeyManagementClient,
	cryptokey *kmspb.CryptoKey, conditionFunc func(key *kmspb.CryptoKey) bool) error {
	r := retry.NewConstant(5 * time.Second)
	r = retry.WithMaxDuration(5*time.Minute, r)

	err := retry.Do(ctx, r, func(ctx context.Context) error {
		key, err := client.GetCryptoKey(ctx, &kmspb.GetCryptoKeyRequest{
			Name: cryptokey.GetName(),
		})

		if err != nil {
			return retry.RetryableError(err) //nolint:wrapcheck
		}

		if conditionFunc(key) {
			return nil
		}

		return retry.RetryableError(errInvalidResource) //nolint:wrapcheck
	})

	if err != nil {
		return fmt.Errorf("timed out while awaiting condition: %w", err)
	}

	return nil
}
