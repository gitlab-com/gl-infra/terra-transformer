package importers

import (
	"errors"
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var awsIAMGroupImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := iam.New(sess)

	output, err := svc.GetGroup(&iam.GetGroupInput{GroupName: &plannedValue})

	if err != nil {
		var awsErr awserr.Error
		if errors.As(err, &awsErr) && awsErr.Code() == iam.ErrCodeNoSuchEntityException {
			// Not found, don't import
			return "", nil
		}

		return "", fmt.Errorf("error getting IAM Group: %w", err)
	}

	return *output.Group.GroupName, nil
})
