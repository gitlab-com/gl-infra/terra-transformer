package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var awsS3BucketImporter = awsImporter("bucket", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := s3.New(sess)

	var foundResource *s3.Bucket

	output, err := svc.ListBuckets(&s3.ListBucketsInput{})
	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	for _, p := range output.Buckets {
		if *p.Name == plannedValue {
			foundResource = p
			break
		}
	}

	if foundResource != nil {
		// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#import
		// S3 bucket can be imported using the bucket, e.g.,
		// terraform import aws_s3_bucket.bucket bucket-name
		return *foundResource.Name, nil
	}

	return "", nil
})
