package importers

import (
	"errors"
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

// IAM group policy attachments can be imported using the group name and policy arn separated by /.
var awsIAMGroupPolicyAttachmentImporter = func(plannedResource *tfjson.StateResource, awsProfile string) (string, error) {
	groupIntf := plannedResource.AttributeValues["group"]
	group, ok := groupIntf.(string)

	if !ok {
		return "", fmt.Errorf("planned resource does not have a `group` attribute: %w", ErrUnknownAttribute)
	}

	policyArnIntf := plannedResource.AttributeValues["policy_arn"]
	policyArn, ok := policyArnIntf.(string)

	if !ok {
		return "", fmt.Errorf("planned resource does not have a `policy_arn` attribute: %w", ErrUnknownAttribute)
	}

	sess, err := session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			CredentialsChainVerboseErrors: aws.Bool(true),
		},
		Profile: awsProfile,
	})
	if err != nil {
		return "", fmt.Errorf("unable to establish AWS session: %w", err)
	}

	svc := iam.New(sess)

	input := &iam.ListAttachedGroupPoliciesInput{
		GroupName: &group,
	}

	isAttached := false
	err = svc.ListAttachedGroupPoliciesPages(input, func(output *iam.ListAttachedGroupPoliciesOutput, lastPage bool) bool {
		for _, p := range output.AttachedPolicies {
			if *p.PolicyArn == policyArn {
				isAttached = true
				return false
			}
		}

		return true
	})

	if err != nil {
		var awsErr awserr.Error
		if errors.As(err, &awsErr) && awsErr.Code() == iam.ErrCodeNoSuchEntityException {
			// Not found, don't import
			return "", nil
		}

		return "", fmt.Errorf("unable to list attached group policies: %w", err)
	}

	if isAttached {
		return fmt.Sprintf("%s/%s", group, policyArn), nil
	}

	return "", nil
}
