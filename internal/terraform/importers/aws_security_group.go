package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

var awsSecurityGroupImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := ec2.New(sess)

	var foundResource *string

	err := svc.DescribeSecurityGroupsPages(&ec2.DescribeSecurityGroupsInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("group-name"),
				Values: []*string{aws.String(plannedValue)},
			},
		},
	}, func(output *ec2.DescribeSecurityGroupsOutput, lastPage bool) bool {
		for _, p := range output.SecurityGroups {
			if *p.GroupName == plannedValue {
				foundResource = p.GroupId
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		return *foundResource, nil
	}

	return "", nil
})
