package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

var awsVpcEndpointImporter = awsImporter("service_name", func(sess *session.Session, plannedValue string,
	plannedResource *tfjson.StateResource) (string, error) {
	svc := ec2.New(sess)

	var foundResource *string

	err := svc.DescribeVpcEndpointsPages(&ec2.DescribeVpcEndpointsInput{}, func(output *ec2.DescribeVpcEndpointsOutput, lastPage bool) bool {
		for _, p := range output.VpcEndpoints {
			if *p.ServiceName == plannedValue {
				foundResource = p.VpcEndpointId
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		return *foundResource, nil
	}

	return "", nil
})
