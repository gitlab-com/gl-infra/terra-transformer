package importers

import (
	"fmt"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var awsDynamoDBImporter = awsImporter("name", func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
	svc := dynamodb.New(sess)

	var foundResource *string

	err := svc.ListTablesPages(&dynamodb.ListTablesInput{}, func(output *dynamodb.ListTablesOutput, lastPage bool) bool {
		for _, p := range output.TableNames {
			if *p == plannedValue {
				foundResource = p
				return false
			}
		}

		return true
	})

	if err != nil {
		return "", fmt.Errorf("resource query failed: %w", err)
	}

	if foundResource != nil {
		// https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table#import
		// DynamoDB table can be imported using the table name, e.g.,
		// terraform import aws_dynamodb_table.mytable mytable
		return *foundResource, nil
	}

	return "", nil
})
