package importers

var finderLookup = map[string]Importer{
	"aws_dynamodb_table":              awsDynamoDBImporter,
	"aws_iam_group_policy_attachment": awsIAMGroupPolicyAttachmentImporter,
	"aws_iam_group":                   awsIAMGroupImporter,
	"aws_iam_openid_connect_provider": awsIAMOpenIDConnectProviderImporter,
	"aws_iam_policy":                  awsIAMPolicyImporter,
	"aws_iam_role":                    awsIAMRoleImporter,
	"aws_iam_role_policy_attachment":  awsIAMRolePolicyAttachmentImporter,
	"aws_iam_service_linked_role":     awsIAMServiceLinkedRoleImporter,
	"aws_kms_key":                     awsKMSKeyImporter,
	"aws_vpc_endpoint":                awsVpcEndpointImporter,
	"aws_route53_zone":                awsRoute53ZoneImporter,
	"aws_security_group":              awsSecurityGroupImporter,
	"aws_s3_bucket":                   awsS3BucketImporter,

	"google_kms_key_ring":   googleKMSKeyRingImporter,
	"google_kms_crypto_key": googleKMSKeyImporter,
}

// FindImporterForType will return the appropriate finder for a given Terraform resource type.
func FindImporterForType(resourceType string) Importer {
	return finderLookup[resourceType]
}
