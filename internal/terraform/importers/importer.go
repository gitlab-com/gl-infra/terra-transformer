package importers

import (
	tfjson "github.com/hashicorp/terraform-json"
)

// Importer aids with importing a single resource type. Given a planned resource, will return the identifier to import.
type Importer func(plannedResource *tfjson.StateResource, awsProfile string) (string, error)
