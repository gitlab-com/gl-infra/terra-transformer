package importers

import (
	"fmt"
	"net/url"
	"strings"

	tfjson "github.com/hashicorp/terraform-json"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/iam"
)

var awsIAMOpenIDConnectProviderImporter = awsImporter("url",
	func(sess *session.Session, plannedValue string, plannedResource *tfjson.StateResource) (string, error) {
		u, err := url.Parse(plannedValue)
		if err != nil {
			return "", fmt.Errorf("unable to parse url: %w", err)
		}

		suffix := "oidc-provider/" + u.Hostname()

		svc := iam.New(sess)

		list, err := svc.ListOpenIDConnectProviders(&iam.ListOpenIDConnectProvidersInput{})
		if err != nil {
			return "", fmt.Errorf("error listing OpenID Connect Providers: %w", err)
		}

		for _, p := range list.OpenIDConnectProviderList {
			if strings.HasSuffix(*p.Arn, suffix) {
				return *p.Arn, nil
			}
		}

		return "", nil
	})
