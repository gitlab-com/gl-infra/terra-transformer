package terraform

import (
	"errors"
	"fmt"
	"log"
	"strings"

	tfjson "github.com/hashicorp/terraform-json"

	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform/importers"
)

var errNoImporterDefined = errors.New("no importer for type")

// Import will import a resource into Terraform given a plan.
func Import(plan *tfjson.Plan, addr string, extraArguments []string, awsProfile string, dryRun bool, skipUnknown bool) error {
	priorState := findInState(plan.PriorState, addr)
	if priorState != nil {
		log.Printf("import: %s already exists in state, skipping", addr)
		return nil
	}

	plannedState := findResourceRecursive(plan.PlannedValues.RootModule, addr)
	if plannedState == nil {
		log.Printf("import: %s does not exist in planned values, skipping", addr)
		return nil
	}

	importer := importers.FindImporterForType(plannedState.Type)
	if importer == nil {
		return fmt.Errorf("import: no importer for %s: %w", plannedState.Type, errNoImporterDefined)
	}

	importIdentifier, err := importer(plannedState, awsProfile)
	if skipUnknown && errors.Is(err, importers.ErrUnknownAttribute) {
		log.Printf("import: %s contains unknown attributes and skip-unknown is set, skipping", addr)
		return nil
	}

	if err != nil {
		return fmt.Errorf("import: importer failed while finding resource %s: %w", addr, err)
	}

	if importIdentifier == "" {
		log.Printf("import: %s not found in cloud account, skipping", addr)
		return nil
	}

	arguments := []string{"import"}
	arguments = append(arguments, extraArguments...)
	arguments = append(arguments, addr, importIdentifier)

	log.Printf("Going to import %s", importIdentifier)
	log.Printf("$ terraform %s", strings.Join(arguments, " "))

	if dryRun {
		return nil
	}

	err = RunTerraform(arguments)
	if err != nil {
		return fmt.Errorf("import: terraform import failed for %s: %w", addr, err)
	}

	return nil
}
