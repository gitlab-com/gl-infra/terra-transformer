package terraform

import (
	tfjson "github.com/hashicorp/terraform-json"
)

func findResourceRecursive(stateModule *tfjson.StateModule, addr string) *tfjson.StateResource {
	for _, resource := range stateModule.Resources {
		if resource.Address == addr {
			return resource
		}
	}

	for _, childModule := range stateModule.ChildModules {
		r := findResourceRecursive(childModule, addr)
		if r != nil {
			return r
		}
	}

	return nil
}

func findInState(state *tfjson.State, addr string) *tfjson.StateResource {
	if state == nil {
		return nil
	}

	return findResourceRecursive(state.Values.RootModule, addr)
}
