package terraform

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var parseFixtures = []struct {
	name     string
	filename string
	expected []Move
	wantErr  bool
}{
	{
		name:     "moves",
		filename: "fixtures/moves.tf",
		expected: []Move{
			{
				From: "aws_backup_plan.rds_backup_plan",
				To:   "module.provisional_regional_r0.aws_backup_plan.rds_backup_plan",
			},
		},
		wantErr: false,
	},
}

func TestParseMovedBlocks(t *testing.T) {
	t.Parallel()

	for _, tt := range parseFixtures {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			result, err := ParseMovedBlocks(tt.filename)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, tt.expected, result)
		})
	}
}
