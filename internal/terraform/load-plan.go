package terraform

import (
	"fmt"
	"io"
	"os"

	tfjson "github.com/hashicorp/terraform-json"
)

// LoadPlan will load a Terraform plan from a file.
func LoadPlan(file string) (*tfjson.Plan, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("unable to open %s: %w", file, err)
	}

	defer f.Close()

	contents, err := io.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("unable to read file %s: %w", file, err)
	}

	var p tfjson.Plan

	err = p.UnmarshalJSON(contents)
	if err != nil {
		return nil, fmt.Errorf("unable to marshall plan %s to Terraform plan: %w", file, err)
	}

	return &p, nil
}
