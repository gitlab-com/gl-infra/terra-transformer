package terraform

import (
	"errors"
	"fmt"
	"os"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclsyntax"
)

var errMovesParseFailed = errors.New("failed to parse move blocks")

// Move represents a Terraform moved refactor.
type Move struct {
	From string
	To   string
}

// ParseMovedBlocks will parse all moved blocks within a Terraform file.
func ParseMovedBlocks(filename string) ([]Move, error) {
	result := []Move{}

	tfFileContents, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("unable to read from stdin: %w", err)
	}

	file, diags := hclsyntax.ParseConfig(tfFileContents, filename, hcl.Pos{Line: 1, Column: 1})
	if diags.HasErrors() {
		return nil, fmt.Errorf("unable to parse HCL: %w", diags)
	}

	body, ok := file.Body.(*hclsyntax.Body)
	if !ok {
		return nil, fmt.Errorf("convert file body to body type: %w", errMovesParseFailed)
	}

	for _, block := range body.Blocks {
		if block.Type != "moved" {
			continue
		}

		m := block.Body.Attributes
		fromAttr := m["from"]
		toAttr := m["to"]

		fromScopeTraversal, ok := fromAttr.Expr.(*hclsyntax.ScopeTraversalExpr)
		if !ok {
			return nil, fmt.Errorf("from is not a scope traversal: %w", errMovesParseFailed)
		}

		toScopeTraversal, ok := toAttr.Expr.(*hclsyntax.ScopeTraversalExpr)
		if !ok {
			return nil, fmt.Errorf("from is not a scope traversal: %w", errMovesParseFailed)
		}

		move := Move{
			From: rangeSource(tfFileContents, fromScopeTraversal.Range()),
			To:   rangeSource(tfFileContents, toScopeTraversal.Range()),
		}

		result = append(result, move)
	}

	return result, nil
}

func rangeSource(bytes []byte, r hcl.Range) string {
	return string(bytes[r.Start.Byte:r.End.Byte])
}
