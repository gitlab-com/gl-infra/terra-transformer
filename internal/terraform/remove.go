package terraform

import (
	"fmt"
	"log"
	"strings"

	tfjson "github.com/hashicorp/terraform-json"
)

// Remove will remove an item from state only if it exists.
func Remove(plan *tfjson.Plan, addr string, dryRun bool) error {
	priorState := findInState(plan.PriorState, addr)
	if priorState == nil {
		log.Printf("remove: %s does not exists in state, skipping removal", addr)
		return nil
	}

	arguments := []string{"state", "rm"}
	arguments = append(arguments, addr)

	log.Printf("Going to remove %s", addr)
	log.Printf("$ terraform %s", strings.Join(arguments, " "))

	if dryRun {
		return nil
	}

	err := RunTerraform(arguments)
	if err != nil {
		return fmt.Errorf("import: terraform remove failed for %s: %w", addr, err)
	}

	return nil
}
