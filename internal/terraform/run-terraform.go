package terraform

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
)

var ErrNonZeroExitCode = errors.New("process returned non-zero exit code")

// RunTerraform will run a terraform command with supplied arguments.
func RunTerraform(args []string) error {
	return RunTerraformWithOutput(args, os.Stdout)
}

// RunTerraformWriteOutputToFile will run a terraform command with supplied arguments and
// write stdout to the provided file.
func RunTerraformWriteOutputToFile(args []string, outputFile string) error {
	out, err := os.Create(outputFile)
	if err != nil {
		return fmt.Errorf("unable to create output file %s: %w", outputFile, err)
	}

	defer func() {
		err := out.Close()
		if err != nil {
			log.Printf("failed to close output %s: %v", outputFile, err)
		}
	}()

	return RunTerraformWithOutput(args, out)
}

// RunTerraformWithOutput will run a terraform command with supplied arguments
// and emit stdout to a io.Writer.
func RunTerraformWithOutput(args []string, out io.Writer) error {
	log.Println("executing: terraform " + strings.Join(args, " "))
	cmd := exec.Command("terraform", args...)

	cmd.Stdout = out
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin

	// Start the wrapped process
	err := cmd.Start()
	if err != nil {
		return fmt.Errorf("unable to run terraform: %w", err)
	}

	// Setup a signal handler for interrupt signals
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)

	// Clean up signals handling
	defer func() {
		signal.Stop(sigChan)
		close(sigChan)
	}()

	go func() {
		sig := <-sigChan
		if sig == nil {
			// Channel has been closed
			return
		}

		log.Printf("received signal %s while running terraform, forwarding to process", sig)

		err = cmd.Process.Signal(sig)
		if err != nil {
			log.Printf("error forwarding signal %s to process: %s", sig, err)
		}
	}()

	err = cmd.Wait()

	if err != nil {
		exitStatus := getExitStatus(err)

		if exitStatus >= 0 {
			// Log the exit code for easier debugging of failed commands.
			log.Printf("terraform returned an exit code of %d", exitStatus)

			return fmt.Errorf("unable to run terraform: process returned status code %d: %w", exitStatus, ErrNonZeroExitCode)
		}

		return fmt.Errorf("unable to run terraform: %w", err)
	}

	return nil
}

func getExitStatus(err error) int {
	var execExitError *exec.ExitError
	if errors.As(err, &execExitError) {
		if status, ok := execExitError.Sys().(syscall.WaitStatus); ok {
			return status.ExitStatus()
		}
	}

	return -1
}
