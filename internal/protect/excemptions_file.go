package protect

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

type exemptionFile struct {
	Exemptions []string `yaml:"exemptions"`
}

var errExemptionDirLookupFailed = errors.New("unable to scan exemption-dir")

// ListExemptionsInDirectory will find all exemptions in a directory.
// looking for all files in that directory with an `.exemption.yml` suffix.
// Returns a list of addresses that are exempt from protection.
func ListExemptionsInDirectory(dir string) ([]string, error) {
	dirStat, err := os.Stat(dir)
	if err != nil {
		return nil, fmt.Errorf("unable to stat dir %s: %w", dir, err)
	}

	// Check that the exemption-dir is a directory
	if !dirStat.IsDir() {
		return nil, fmt.Errorf("exemption-dir %s is not a directory: %w", dir, errExemptionDirLookupFailed)
	}

	results := []string{}

	exemptionFiles, err := filepath.Glob(dir + "/*.exemption.yml")
	if err != nil {
		return nil, fmt.Errorf("unable to find exemption files: %w", err)
	}

	for _, v := range exemptionFiles {
		exemptions, err := listExemptionsInFile(v)

		if err != nil {
			return nil, fmt.Errorf("unable to parse exemptions in file %s: %w", v, err)
		}

		results = append(results, exemptions...)
	}

	return results, nil
}

func listExemptionsInFile(fileName string) ([]string, error) {
	data, err := os.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("unable to read file: %s: %w", fileName, err)
	}

	t := exemptionFile{}

	err = yaml.Unmarshal(data, &t)
	if err != nil {
		return nil, fmt.Errorf("unable to parse yaml in %s: %w", fileName, err)
	}

	exemptions := expandEnvironmentVariables(t.Exemptions)

	return exemptions, nil
}

// expandEnvironmentVariables will expand environment variables in a list of exemption addresses.
func expandEnvironmentVariables(exemptions []string) []string {
	result := make([]string, len(exemptions))

	for k, v := range exemptions {
		result[k] = os.ExpandEnv(v)
	}

	return result
}
