package protect

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListExemptionsInDirectory(t *testing.T) {
	t.Parallel()

	var expectedExemptions = []string{
		"aws_ec2_instance.foo",
		"aws_ec2_instance.bar",
		"module.abc.aws_secretsmanager_secret.foo",
		"module.abc.aws_secretsmanager_secret.bar",
	}

	got, err := ListExemptionsInDirectory("./fixtures/basic")
	require.NoError(t, err)

	require.Equal(t, expectedExemptions, got)
}

// Can't use parallel tests with t.Setenv
//
//nolint:paralleltest
func TestListExemptionsInDirectoryWithEnvExpansions(t *testing.T) {
	t.Setenv("TENANT_ID", "mu-mu-land")

	var expectedExemptions = []string{
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["mu-mu-land-consul-1-logs"]`,
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["mu-mu-land-consul-2-logs"]`,
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["mu-mu-land-consul-3-logs"]`,
	}

	got, err := ListExemptionsInDirectory("./fixtures/env-expansion")
	require.NoError(t, err)

	require.Equal(t, expectedExemptions, got)
}

// Can't use parallel tests with t.Setenv
//
//nolint:paralleltest
func TestListExemptionsInDirectoryWithBlankEnvExpansions(t *testing.T) {
	t.Setenv("TENANT_ID", "")

	var expectedExemptions = []string{
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["-consul-1-logs"]`,
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["-consul-2-logs"]`,
		`module.provisional_regional_r0.module.get.module.item.module.consul.aws_ebs_volume.gitlab["-consul-3-logs"]`,
	}

	got, err := ListExemptionsInDirectory("./fixtures/env-expansion")
	require.NoError(t, err)

	require.Equal(t, expectedExemptions, got)
}
