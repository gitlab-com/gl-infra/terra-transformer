package protect

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/viper"

	"gitlab.com/gitlab-com/gl-infra/terra-transformer/internal/terraform"
)

// ErrNoFilters will be thrown when `terra-transform protect` is called without
// any filter types, either supplied via the config file, or command line arguments.
var ErrNoFilters = errors.New("protect command needs types specified")

// Apply will apply protections and exceptions.
// Use allowNoFilters to control whether an empty config is acceptable.
func Apply(planFile string, exemptionTypes []string, allowNoFilters bool, exemptionsDirectory string) error {
	protectedTypes := viper.GetStringSlice("protected_types")
	protectedTypes = append(protectedTypes, exemptionTypes...)

	if len(protectedTypes) == 0 {
		if allowNoFilters {
			return nil
		}

		return ErrNoFilters
	}

	p, err := terraform.LoadPlan(planFile)
	if err != nil {
		return fmt.Errorf("unable to load plan: %w", err)
	}

	protectedResources := FindProtectedResourcesInPlan(p, protectedTypes)

	protectedResources, err = FilterProtectionExceptions(protectedResources, exemptionsDirectory)
	if err != nil {
		return fmt.Errorf("invalid exemptions: %w", err)
	}

	if len(protectedResources) > 0 {
		fmt.Println("-------------------------------------------------------------------------")
		fmt.Println("⚠️  Terraform Plan contains changes that will delete protected resources.")
		fmt.Println()
		fmt.Println("The following protected resources will be deleted:")
		fmt.Println()

		for _, v := range protectedResources {
			fmt.Printf("- %s\n", v.Address)
		}

		fmt.Println()
		fmt.Println("⚠️  This may lead to loss of critical data. Manual intervention required.")
		fmt.Println("-------------------------------------------------------------------------")
		fmt.Println("Note: if these resources can be safely deleted, follow the instructions for adding an exemption at")
		fmt.Println("https://gitlab.com/gitlab-com/gl-infra/terra-transformer/-/blob/main/README.md#exemption")

		os.Exit(2)
	}

	return nil
}
