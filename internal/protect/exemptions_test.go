package protect

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

var fixtures = []struct {
	name            string
	exemptionString string
	want            []string
	wantErr         bool
}{
	{
		name:            "basic",
		exemptionString: "2022-05-15T00:00:00Z,2h=aws_rds_instance",
		want:            []string{"aws_rds_instance"},
		wantErr:         false,
	},
	{
		name:            "wrong_day_before",
		exemptionString: "2022-05-14T00:00:00Z,2h=aws_rds_instance",
		want:            nil,
		wantErr:         true,
	},
	{
		name:            "wrong_day_after",
		exemptionString: "2022-05-16T00:00:00Z,2h=aws_rds_instance",
		want:            nil,
		wantErr:         true,
	},
	{
		name:            "exemption_too_long",
		exemptionString: "2022-05-14T00:00:00Z,2d=aws_rds_instance",
		want:            nil,
		wantErr:         true,
	},
	{
		name:            "no_duration",
		exemptionString: "2022-05-14T00:00:00Z=aws_rds_instance",
		want:            nil,
		wantErr:         true,
	},
	{
		name:            "no_resources",
		exemptionString: "2022-05-14T00:00:00Z,2h=",
		want:            nil,
		wantErr:         true,
	},
}

func Test_exemptionParser(t *testing.T) {
	t.Parallel()

	for _, tt := range fixtures {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			evaluationTime, err := time.Parse(time.RFC3339, "2022-05-15T00:00:00Z")
			require.NoError(t, err)

			got, err := exemptionParser(tt.exemptionString, evaluationTime)
			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, tt.want, got)
		})
	}
}
