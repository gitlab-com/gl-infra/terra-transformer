package protect

import (
	tfjson "github.com/hashicorp/terraform-json"
)

// FindProtectedResourcesInPlan will find any protected resources in a terraform plan.
func FindProtectedResourcesInPlan(plan *tfjson.Plan, protectedTypes []string) []*tfjson.ResourceChange {
	results := []*tfjson.ResourceChange{}

	protectedTypesMap := map[string]struct{}{}
	for _, v := range protectedTypes {
		protectedTypesMap[v] = struct{}{}
	}

	for _, v := range plan.ResourceChanges {
		if !willDelete(v) {
			continue
		}

		if _, ok := protectedTypesMap[v.Type]; ok {
			results = append(results, v)
			continue
		}
	}

	return results
}

func willDelete(v *tfjson.ResourceChange) bool {
	for _, v := range v.Change.Actions {
		if v == tfjson.ActionDelete {
			return true
		}
	}

	return false
}
