package protect

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	tfjson "github.com/hashicorp/terraform-json"
)

var maxExemptionDuration = 24 * time.Hour

// ErrInvalidExemption is raised when an exemption doesn't parse.
var ErrInvalidExemption = errors.New("invalid exemption definition")

// FilterProtectionExceptions will allow certain temporary exemptions from protection.
// These can be passed through the environment.
func FilterProtectionExceptions(protectedResources []*tfjson.ResourceChange, exemptionsDirectory string) ([]*tfjson.ResourceChange, error) {
	protectedResources, err := filterExemptionsInEnvironment(protectedResources)
	if err != nil {
		return nil, err
	}

	if exemptionsDirectory != "" {
		protectedResources, err = filterExemptionsInDirectory(protectedResources, exemptionsDirectory)
		if err != nil {
			return nil, err
		}
	}

	return protectedResources, nil
}

// filterExemptionsInEnvironment allows transient exemptions to be set through environment variables.
// This is used to filter exemptions by TYPE.
func filterExemptionsInEnvironment(protectedResources []*tfjson.ResourceChange) ([]*tfjson.ResourceChange, error) {
	exemptionDefinition, exists := os.LookupEnv("TERRA_PROTECT_RESOURCE_EXEMPTION")
	if !exists {
		return protectedResources, nil
	}

	log.Printf("Note: use of TERRA_PROTECT_RESOURCE_EXEMPTION is deprecated. Please consider switching over to exemption files.")

	validExemptionTypes, err := exemptionParser(exemptionDefinition, time.Now())
	if err != nil {
		return nil, err
	}

	validExemptionTypesMap := map[string]struct{}{}
	for _, v := range validExemptionTypes {
		validExemptionTypesMap[v] = struct{}{}
	}

	result := []*tfjson.ResourceChange{}

	for _, v := range protectedResources {
		if _, ok := validExemptionTypesMap[v.Type]; ok {
			continue
		}

		result = append(result, v)
	}

	return result, nil
}

// Filter exemptions by ADDRESS from exemption files in a directory.
func filterExemptionsInDirectory(protectedResources []*tfjson.ResourceChange, exemptionsDirectory string) ([]*tfjson.ResourceChange, error) {
	exemptionAddresses, err := ListExemptionsInDirectory(exemptionsDirectory)
	if err != nil {
		return nil, err
	}

	exemptionAddressesMap := map[string]struct{}{}
	for _, v := range exemptionAddresses {
		exemptionAddressesMap[v] = struct{}{}
	}

	result := []*tfjson.ResourceChange{}

	for _, v := range protectedResources {
		if _, ok := exemptionAddressesMap[v.Address]; ok {
			continue
		}

		result = append(result, v)
	}

	return result, nil
}

// Parses exemptions in the form
// 2022-05-15T00:00:00Z,2h=aws_instance,aws_rds_instance.
func exemptionParser(exemptionString string, evaluationTime time.Time) ([]string, error) {
	p1 := strings.SplitN(exemptionString, "=", 2)
	if len(p1) != 2 {
		return nil, ErrInvalidExemption
	}

	p2 := strings.SplitN(p1[0], ",", 2)
	if len(p2) != 2 {
		return nil, ErrInvalidExemption
	}

	exemptResources := strings.Split(p1[1], ",")

	if len(p2) < 1 {
		return nil, ErrInvalidExemption
	}

	timeStr := p2[0]
	durationStr := p2[1]

	startTime, err := time.Parse(time.RFC3339, timeStr)
	if err != nil {
		return nil, fmt.Errorf("invalid time: %w", ErrInvalidExemption)
	}

	exemptionDuration, err := time.ParseDuration(durationStr)
	if err != nil {
		return nil, fmt.Errorf("invalid duration: %w", ErrInvalidExemption)
	}

	if exemptionDuration > maxExemptionDuration {
		return nil, fmt.Errorf("exemption duration exceeds maximum allowed: %w", ErrInvalidExemption)
	}

	endTime := startTime.Add(exemptionDuration)

	if evaluationTime.Before(startTime) || evaluationTime.After(endTime) {
		return nil, fmt.Errorf("exemption period exceeded: %w", ErrInvalidExemption)
	}

	return exemptResources, nil
}
