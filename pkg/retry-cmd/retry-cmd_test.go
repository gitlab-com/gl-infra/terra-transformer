package retry

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
)

type mockFuncWithAttempts func(attempts *uint) func(cmd *cobra.Command, args []string) error

var errMock = errors.New("failed with error")

func succeedAfterTimes(attemptsBeforeSuccess uint) mockFuncWithAttempts {
	return func(attempts *uint) func(cmd *cobra.Command, args []string) error {
		return func(cmd *cobra.Command, args []string) error {
			*attempts = *attempts + 1

			if *attempts < attemptsBeforeSuccess {
				return errMock
			}

			return nil
		}
	}
}

func TestCmd(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name          string
		maxRetries    uint
		cmdFunc       mockFuncWithAttempts
		attemptsCount uint
		wantErr       bool
	}{
		{
			name:          "zero retries returns error",
			maxRetries:    0,
			cmdFunc:       succeedAfterTimes(0), // Always pass
			attemptsCount: 0,
			wantErr:       true,
		},
		{
			name:          "successful execution on first try",
			maxRetries:    3,
			cmdFunc:       succeedAfterTimes(1),
			attemptsCount: 1,
			wantErr:       false,
		},
		{
			name:          "succeeds after 2 retries",
			maxRetries:    3,
			cmdFunc:       succeedAfterTimes(2),
			attemptsCount: 2,
			wantErr:       false,
		},
		{
			name:          "fails after all retries",
			maxRetries:    3,
			cmdFunc:       succeedAfterTimes(100),
			attemptsCount: 3,
			wantErr:       true,
		},
		{
			name:          "succeeds after a lot of retries",
			maxRetries:    100,
			cmdFunc:       succeedAfterTimes(30),
			attemptsCount: 30,
			wantErr:       false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			cmd := &cobra.Command{
				Use: "test",
			}
			cmd.SetContext(context.Background())

			attempts := uint(0)
			fn := tt.cmdFunc(&attempts)

			// Keep this small for tests
			initialDelay := 1 * time.Microsecond

			retryFunc := Cmd(fn, WithMaxRetries(&tt.maxRetries), WithInitialDelay(&initialDelay))
			err := retryFunc(cmd, []string{})

			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			require.Equal(t, tt.attemptsCount, attempts, "Expected attempts did not match actual attempts")
		})
	}
}
