package retry

import (
	"errors"
	"fmt"
	"log"
	"time"

	retry "github.com/avast/retry-go/v4"
	"github.com/spf13/cobra"
)

var errInvalidRetryConfiguration = errors.New("invalid retry configuration")

type retryConfiguration struct {
	maxRetries   *uint
	initialDelay *time.Duration
}

type option func(c *retryConfiguration)

func WithMaxRetries(maxRetries *uint) option {
	return func(c *retryConfiguration) {
		c.maxRetries = maxRetries
	}
}

func WithInitialDelay(initialDelay *time.Duration) option {
	return func(c *retryConfiguration) {
		c.initialDelay = initialDelay
	}
}

func apply(options []option) *retryConfiguration {
	cfg := &retryConfiguration{}
	for _, option := range options {
		option(cfg)
	}

	return cfg
}

type CobraRunFunc func(cmd *cobra.Command, args []string) error

// Wrap a DelayTypeFunc to allow us to log the duration until the next retry.
func loggingDelay(delay retry.DelayTypeFunc, maxDelay time.Duration) retry.DelayTypeFunc {
	return func(n uint, err error, config *retry.Config) time.Duration {
		d := delay(n, err, config)

		// maxDuration is applied after this function, but we
		// can guess what the outcome is going to be and report
		// that, as it's more accurate that using the uncapped value
		// which continues to increase in an unlimited fashion
		var estimated time.Duration

		if d > maxDelay {
			estimated = maxDelay
		} else {
			estimated = d
		}

		log.Printf("Command failed on attempt %d: %v. Waiting %s before retrying", n, err, estimated)

		return d
	}
}

func Cmd(f CobraRunFunc, options ...option) CobraRunFunc {
	return func(cmd *cobra.Command, args []string) error {
		cfg := apply(options)

		if *cfg.maxRetries == 0 {
			return fmt.Errorf("max-retries cannot be 0: %w", errInvalidRetryConfiguration)
		}

		maxDelay := *cfg.initialDelay * 10

		err := retry.Do(func() error {
			return f(cmd, args)
		},
			retry.Context(cmd.Context()),
			retry.Attempts(*cfg.maxRetries),
			retry.DelayType(loggingDelay(retry.CombineDelay(retry.BackOffDelay, retry.RandomDelay), maxDelay)),

			retry.Delay(*cfg.initialDelay),
			retry.MaxDelay(maxDelay),
			retry.MaxJitter(*cfg.initialDelay/6),
		)

		return err //nolint:wrapcheck
	}
}
